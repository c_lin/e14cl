#include <iostream>

#include "LcConfig.h"

int main( int argc, char **argv )
{
   std::cout<<" Welcome! This is e14cl version " 
            << e14cl_VERSION_MAJOR <<"."
            << e14cl_VERSION_MINOR <<"."
            << e14cl_VERSION_PATCH <<"." << std::endl;
 
   std::cout<<" number of arguments = " << argc << std::endl;
   return 1;
}
